---
layout: post
section: others
title: La Jaquería
---

## Información

La Jaquería es una organización de naturaleza asociativa sin ánimo de lucro, con fines culturales, situada en Almería. Sus principales finalidades son, resumiendo, promover la experimentación y el conocimiento tecnológico, científico, y artístico a través de la colaboración y educación social sirviendo de punto de difusión de la llamada cultura maker, o conjunto de prácticas basadas en el “hágalo por si mismo” herederas de la ética hacker y la experimentación en los procesos de creación y fabricación digital (diseño global, fabricación local), acercando la Ciencia y la Tecnología a la ciudadanía para fomentar la cultura científica y el racionalismo.

-   Temáticas principales: cultura maker

## Contacto

-   Sede física: C/ La Vega, 44 04007 Almería
-   Página web: <https://lajaqueria.org/>
-   Email: <lajaqueria@gmail.com>
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/LaJaqueria>
-   Sala en Matrix:
-   Grupo o canal en Telegram:
-   GitLab/GitHub (u otros sitios de código colaborativo): <https://github.com/LaJaqueria>

---
layout: post
section: associations
title: Asociación de Estudiantes de Software Libre de la Universidad Pablo de Olavide
---

## Información

-   Universidad a la que está asociada: Universidad Pablo de Olavide
-   Activa en la actualidad: Sí

## Contacto

-   Sede física: Universidad Pablo de Olavide. Carretera de Utrera Km 1, 41013 Sevilla, Sevilla
-   Página web: <https://esoliupo.es/>
-   Email: <info@esoliupo.es>
-   Mastodon (u otras redes sociales libres): No procede
-   Twitter: <https://twitter.com/esoliupo>
-   Instagram: <https://instagram.com/esoliupo>
-   LinkedIn: <https://www.linkedin.com/company/esoliupo/>
-   Sala en Matrix: No procede
-   Grupo o canal en Telegram: <https://t.me/asocesoliupo>
-   GitLab/GitHub (u otros sitios de código colaborativo): <https://github.com/Asociacion-ESOLIUPO/>

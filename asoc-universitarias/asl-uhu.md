---
layout: post
section: associations
title: Asociación de Software Libre de la Universidad de Huelva
---

## Información

-   Universidad a la que está asociada: Universidad de Huelva
-   Activa en la actualidad: Parcialmente

## Contacto

-   Sede física:
-   Página web:
-   Email:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/asl_uhu>
-   Sala en Matrix:
-   Grupo o canal en Telegram:
-   GitLab/GitHub (u otros sitios de código colaborativo):

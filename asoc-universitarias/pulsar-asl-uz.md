---
layout: post
section: associations
title: Púlsar - Asociación de Software Libre de la Universidad de Zaragoza
---

## Información

-   Universidad a la que está asociada: Universidad de Zaragoza
-   Activa en la actualidad: No

## Contacto

-   Sede física:
-   Página web: <https://www.pulsar.unizar.es/>
-   Email:
-   Mastodon (u otras redes sociales libres):
-   Twitter:
-   Sala en Matrix:
-   Grupo o canal en Telegram:
-   GitLab/GitHub (u otros sitios de código colaborativo):

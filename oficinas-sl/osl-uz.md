---
layout: post
section: offices
title: Oficina de Software Libre de la Universidad de Zaragoza
---

## Información

-   Director/a:
-   Email:
-   Activa en la actualidad: Sí

## Contacto

-   Dirección física:
-   Página web: <https://osluz.unizar.es/>
-   Email:
-   Teléfono:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/osluz_unizar>
-   Sala en Matrix:
-   Grupo o canal en Telegram:
-   GitLab/GitHub (u otros sitios de código colaborativo):

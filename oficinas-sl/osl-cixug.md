---
layout: post
section: offices
title: Consorcio para o Desenvolvemento de Aplicacións de Xestión Universitaria
---

## Información

-   Director/a:
-   Email:
-   Activa en la actualidad: No

## Contacto

-   Dirección física:
-   Página web: <https://osl.cixug.es/>
-   Email:
-   Teléfono:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/cixug_>
-   Sala en Matrix:
-   Grupo o canal en Telegram:
-   GitLab/GitHub (u otros sitios de código colaborativo):

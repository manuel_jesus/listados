---
layout: post
section: offices
title: Oficina de Software Libre de la Universidad Carlos III de Madrid
---

## Información

-   Director/a:
-   Email:
-   Activa en la actualidad: No

## Contacto

-   Dirección física:
-   Página web: <https://osl.uc3m.es/>
-   Email:
-   Teléfono:
-   Mastodon (u otras redes sociales libres):
-   Twitter:
-   Sala en Matrix:
-   Grupo o canal en Telegram:
-   GitLab/GitHub (u otros sitios de código colaborativo):

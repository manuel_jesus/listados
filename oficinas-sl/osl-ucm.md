---
layout: post
section: offices
title: Oficina de Software Libre de la Universidad Complutense de Madrid
---

## Información

-   Director/a:
-   Email:
-   Activa en la actualidad: Sí

## Contacto

-   Dirección física:
-   Página web: <https://www.ucm.es/oficina-de-software-libre/>
-   Email:
-   Teléfono:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/oteaucm>
-   Sala en Matrix:
-   Grupo o canal en Telegram:
-   GitLab/GitHub (u otros sitios de código colaborativo):

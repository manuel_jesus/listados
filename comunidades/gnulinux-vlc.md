---
layout: post
section: communities
title: GNU/Linux Valencia
---

## Información

-   Temáticas principales: Software Libre en general, Educación, Ética libre

## Contacto

-   Sede física: Valencia
-   Página web: <https://gnulinuxvalencia.org/>
-   Email: correo(arroba)gnulinuxvalencia(punto)org
-   Mastodon (u otras redes sociales libres): [Mastodon](https://floss.social/@gnulinuxvalencia) - [PeerTube](https://devtube.dev-wiki.de/accounts/gnulinuxvalencia/video-channels)
-   Twitter: <https://twitter.com/GnuLinuxVlc>
-   Sala en Matrix: gnulinuxvalencia.org:matrix.org
-   Grupo o canal en Telegram: https://t.me/canalgnulinuxvalencia
-   GitLab/GitHub (u otros sitios de código colaborativo): 
